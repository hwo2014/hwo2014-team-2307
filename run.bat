@echo off
cd mono

echo ==== Compiling ====
echo.
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\csc.exe /debug:pdbonly /define:LOCAL /nologo /out:bot.exe -r:Newtonsoft.Json.dll *.cs
echo.
if %errorlevel% neq 0 goto comp_fail

echo ==== Running ====
echo.
bot.exe testserver.helloworldopen.com 8091 "Scientific Method" "IFcWJvDJXTGtuA"
echo.
if %errorlevel% neq 0 goto prog_fail

goto end

:comp_fail
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo !!!! Compilation failed !!!!
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo.
goto end

:prog_fail
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo !!!! Program execution failed !!!!
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo.
goto end

:end
pause