using System;
using System.Collections.Generic;

class StraightPiece : Piece
{
    private readonly double length;
    private readonly IList<Lane> lanes;

    public StraightPiece(double length, bool hasSwitch, IList<Lane> lanes) : base(hasSwitch)
    {
        this.length = length;
        this.lanes = lanes;
    }

    public double Length
    {
        get
        {
            return this.length;
        }
    }

    public override double? GetCurvature(int startLane, int endLane, double dist)
    {
        return 0.0;
    }

    public override double GetApproxCurvature(int startLane, int endLane, double dist)
    {
        return 0.0;
    }

    public override double? GetLength(int startLane, int endLane)
    {
        if (startLane == endLane)
        {
            return this.length;
        }
        else
        {
            return LengthCache.GetStraightValue(this.length, Math.Abs(this.lanes[startLane].Offset - this.lanes[endLane].Offset));
        }
    }

    public override double GetApproxLength(int startLane, int endLane)
    {
        var length = this.GetLength(startLane, endLane);
        if (!length.HasValue)
        {
            var delta = this.lanes[startLane].Offset - this.lanes[endLane].Offset;
            length = Math.Sqrt(this.length * this.length + delta * delta);
        }
        return length.Value;
    }

    public override void SetMeasuredLength(int startLane, int endLane, double length)
    {
        if (startLane != endLane)
        {
            LengthCache.SetStraightValue(this.length, Math.Abs(this.lanes[startLane].Offset - this.lanes[endLane].Offset), length);
        }
    }
}