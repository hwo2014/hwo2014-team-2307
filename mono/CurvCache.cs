using System;
using System.Collections.Generic;

class CurvCache
{
    private static readonly Dictionary<Tuple<double, double, double>, List<Tuple<double, double>>> values = new Dictionary<Tuple<double, double, double>, List<Tuple<double, double>>>();
    private static readonly Dictionary<Tuple<double, double, double>, Tuple<double, double, double>> coeffs = new Dictionary<Tuple<double, double, double>, Tuple<double, double, double>>();

    public static void RecordValue(double radius1, double radius2, double angle, double dist, double value)
    {
        var key = new Tuple<double, double, double>(radius1, radius2, angle);
        if (!values.ContainsKey(key))
        {
            values[key] = new List<Tuple<double, double>>();
        }
        values[key].Add(new Tuple<double, double>(dist, value));
        coeffs.Remove(key);
    }

    public static double? GetValue(double radius1, double radius2, double angle, double dist)
    {
        var key = new Tuple<double, double, double>(radius1, radius2, angle);
        if (!coeffs.ContainsKey(key))
        {
            if (!values.ContainsKey(key) || values[key].Count < 3)
            {
                return null;
            }
            var x0 = 0.0;
            var x1 = 0.0;
            var x2 = 0.0;
            var x3 = 0.0;
            var x4 = 0.0;
            var yx0 = 0.0;
            var yx1 = 0.0;
            var yx2 = 0.0;
            foreach (var xy in values[key])
            {
                var x = xy.Item1;
                var y = xy.Item2;
                x0 += 1.0;
                x1 += x;
                x2 += x * x;
                x3 += x * x * x;
                x4 += x * x * x * x;
                yx0 += y;
                yx1 += y * x;
                yx2 += y * x * x;
            }
            var matrix = new double[][] { new double[] { x0, x1, x2, yx0 }, new double[] { x1, x2, x3, yx1 }, new double[] { x2, x3, x4, yx2 } };
            for (var i = 0; i < 3; i++)
            {
                if (!NormRow(matrix[i], i))
                {
                    return null;
                }
                for (var j = 0; j < 3; j++)
                {
                    if (i != j)
                    {
                        SubRow(matrix[j], matrix[i], i);
                    }
                }
            }
            coeffs[key] = new Tuple<double, double, double>(matrix[0][3], matrix[1][3], matrix[2][3]);
        }
        var tuple = coeffs[key];
        return tuple.Item1 + dist * tuple.Item2 + dist * dist * tuple.Item3;
    }

    private static bool NormRow(double[] row, int offset)
    {
        var c = row[offset];
        if (Math.Abs(c) < 1e-9)
        {
            return false;
        }
        row[0] /= c;
        row[1] /= c;
        row[2] /= c;
        row[3] /= c;
        row[offset] = 1.0;
        return true;
    }

    private static void SubRow(double[] row1, double[] row2, int offset)
    {
        var c = row1[offset];
        row1[0] -= row2[0] * c;
        row1[1] -= row2[1] * c;
        row1[2] -= row2[2] * c;
        row1[3] -= row2[3] * c;
        row1[offset] = 0.0;
    }
}