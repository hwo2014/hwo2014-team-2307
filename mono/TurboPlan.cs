using System;
using System.Collections.Generic;

class TurboPlan
{
    private readonly Dictionary<int, int> singleLapStraights = new Dictionary<int, int>();
    private readonly Dictionary<int, int> multiLapStraights = new Dictionary<int, int>();

    public TurboPlan(Track track)
    {
        var maxSingle = 0.0;
        var maxMulti = 0.0;
        for (var i = 0; i < track.Pieces.Count; i++)
        {
            var single = this.SingleLength(track, i);
            if (single.Item1 > maxSingle)
            {
                maxSingle = single.Item1;
                this.singleLapStraights.Clear();
            }
            if (single.Item1 == maxSingle)
            {
                this.singleLapStraights[i] = single.Item2;
            }
            var multi = this.MultiLength(track, i);
            if (multi.Item1 > maxMulti)
            {
                maxMulti = multi.Item1;
                this.multiLapStraights.Clear();
            }
            if (multi.Item1 == maxMulti)
            {
                this.multiLapStraights[i] = multi.Item2;
            }
        }
    }

    private Tuple<double, int> SingleLength(Track track, int start)
    {
        var pieces = track.Pieces;
        var length = 0.0;
        var numPieces = 0;
        for (var i = start; i < pieces.Count; i++)
        {
            var straigth = pieces[i] as StraightPiece;
            if (straigth == null)
            {
                break;
            }
            length += straigth.Length;
            numPieces += 1;
        }
        return new Tuple<double, int>(length, numPieces);
    }

    private Tuple<double, int> MultiLength(Track track, int start)
    {
        var pieces = track.Pieces;
        var length = 0.0;
        var numPieces = 0;
        for (var i = start; i < 2 * pieces.Count; i++)
        {
            var straigth = pieces[i % pieces.Count] as StraightPiece;
            if (straigth == null)
            {
                break;
            }
            length += straigth.Length;
            numPieces += 1;
        }
        return new Tuple<double, int>(length, numPieces);
    }

    public Dictionary<int, int> SingleLapStraights
    {
        get
        {
            return this.singleLapStraights;
        }
    }

    public Dictionary<int, int> MultiLapStraights
    {
        get
        {
            return this.multiLapStraights;
        }
    }
}