using System.Collections.Generic;

class DelegateCommand : Command
{
    private readonly IEnumerable<Command> commands;

    public DelegateCommand(IEnumerable<Command> commands)
    {
        this.commands = commands;
    }

    public override IEnumerable<SendMessage> GetMessages()
    {
        foreach (var command in this.commands)
        {
            foreach (var message in command.GetMessages())
            {
                yield return message;
            }
        }
    }
}