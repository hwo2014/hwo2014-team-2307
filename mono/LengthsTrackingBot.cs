using System.Collections.Generic;
using Newtonsoft.Json.Linq;

abstract class LengthsTrackingBot : SlipAngleTrackingBot
{
    protected override void OnCarPositions(List<JToken> positions)
    {
        base.OnCarPositions(positions);
        var previous = this.History.PreviousPosition;
        if (this.Power != 0.0 && this.Drag != 0.0 && previous != null && previous.Index != this.Position.Index && previous.Speed.HasValue)
        {
            var speed = this.Drag * previous.Speed.Value + this.Power * this.Position.PreviousThrottle * previous.ThrottleFactor;
            this.Position.SetMeasuredSpeed(speed);
        }
    }
}