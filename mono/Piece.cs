abstract class Piece
{
    private readonly bool hasSwitch;

    public Piece(bool hasSwitch)
    {
        this.hasSwitch = hasSwitch;
    }

    public bool HasSwitch
    {
        get
        {
            return this.hasSwitch;
        }
    }

    abstract public double? GetCurvature(int startLane, int endLane, double dist);

    abstract public double GetApproxCurvature(int startLane, int endLane, double dist);

    abstract public double? GetLength(int startLane, int endLane);

    abstract public double GetApproxLength(int startLane, int endLane);

    abstract public void SetMeasuredLength(int startLane, int endLane, double length);
}