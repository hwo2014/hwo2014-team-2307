using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

class RecvMessage
{
    private readonly string type;
    private readonly JToken data;
    private readonly int? tick;

    public RecvMessage(string type, JToken data, int? tick)
    {
        this.type = type;
        this.data = data;
        this.tick = tick;
    }

    public static RecvMessage FromJson(string json)
    {
        var obj = JsonConvert.DeserializeObject(json) as JToken;
        int? tick = null;
        if (obj["gameTick"] != null)
        {
            tick = (int)obj["gameTick"];
        }
        return new RecvMessage((string)obj["msgType"], obj["data"], tick);
    }

    public string Type
    {
        get
        {
            return this.type;
        }
    }

    public JToken Data
    {
        get
        {
            return this.data;
        }
    }

    public int? Tick
    {
        get
        {
            return this.tick;
        }
    }
}