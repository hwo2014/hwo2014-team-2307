using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

class RaceClient : IDisposable
{
    private readonly TcpClient tcpClient;
    private readonly StreamReader reader;
    private readonly StreamWriter writer;
     private readonly Stopwatch timer;
#if LOCAL
    private readonly StreamWriter logger;
#endif

    public RaceClient(string host, int port)
    {
        this.timer = new Stopwatch();
        this.timer.Start();
        this.tcpClient = new TcpClient(host, port);
        this.tcpClient.NoDelay = true;
        var stream = this.tcpClient.GetStream();
        this.reader = new StreamReader(stream);
        this.writer = new StreamWriter(stream);
        this.writer.AutoFlush = true;
#if LOCAL
        var fileName = "log-" + Guid.NewGuid().ToString() + ".txt";
        Console.WriteLine("Logging to {0}", fileName);
        this.logger = new StreamWriter(fileName);
        this.logger.AutoFlush = true;
#endif
    }

    public void Dispose()
    {
        ((IDisposable)this.tcpClient).Dispose();
#if LOCAL
        ((IDisposable)this.logger).Dispose();
#endif
    }

    public async Task Send(SendMessage message)
    {
        var json = message.ToJson();
#if LOCAL
        this.logger.WriteLine(json);
        await this.writer.WriteLineAsync(json);
#else
        Console.WriteLine("{0}", this.timer.ElapsedMilliseconds);
        Console.WriteLine(json);
        this.writer.WriteLine(json);
        Console.WriteLine("{0}", this.timer.ElapsedMilliseconds);
#endif
    }

    public async Task<RecvMessage> Receive()
    {
#if LOCAL
        var json = await this.reader.ReadLineAsync();
#else
        Console.WriteLine("{0}", this.timer.ElapsedMilliseconds);
        var json = this.reader.ReadLine();
#endif
        if (json == null)
        {
#if LOCAL
            this.logger.WriteLine("null");
#else
            Console.WriteLine("null");
            Console.WriteLine("{0}", this.timer.ElapsedMilliseconds);
#endif
            return null;
        }
#if LOCAL
        this.logger.WriteLine(json);
#else
        Console.WriteLine(json);
        Console.WriteLine("{0}", this.timer.ElapsedMilliseconds);
#endif
        return RecvMessage.FromJson(json);
    }
}