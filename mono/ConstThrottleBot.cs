using System.Collections.Generic;

class ConstThrottleBot : CommandsBot
{
    private double throttle;

    public ConstThrottleBot(double throttle)
    {
        this.throttle = throttle;
    }

    protected override IEnumerable<Command> Drive()
    {
        yield return this.Delegate(base.Drive());
        while (true)
        {
            yield return this.SetThrottle(this.throttle);
        }
    }
}