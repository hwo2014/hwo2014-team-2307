using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

abstract class SpawnTimeTrackingBot : TrackingBot
{
    private readonly Dictionary<string, int> crashTime = new Dictionary<string, int>();
    private int? minSpawnTime = null;

    protected override void OnCrash(string name, string color)
    {
        base.OnCrash(name, color);
        this.crashTime[color] = this.Tick;
    }

    protected override void OnGameInit(JToken track, List<JToken> cars, JToken session)
    {
        base.OnGameInit(track, cars, session);
        this.crashTime.Clear();
    }

    protected override void OnSpawn(string name, string color)
    {
        base.OnSpawn(name, color);
        if (this.crashTime.ContainsKey(color))
        {
            var time = this.Tick - this.crashTime[color];
            if (!this.minSpawnTime.HasValue)
            {
                this.minSpawnTime = time;
            }
            else
            {
                this.minSpawnTime = Math.Min(this.minSpawnTime.Value, time);
            }
            this.crashTime.Remove(color);
        }
        else
        {
            this.LogError("Player {0} ({1}) spawned on tick {2} without previously crashing", name, color, this.Tick);
        }
    }

    protected int SpawnTime
    {
        get
        {
            if (this.minSpawnTime.HasValue)
            {
                return this.minSpawnTime.Value;
            }
            else
            {
                return 400;
            }
        }
    }

    protected int GetTimeTillSpawn(string color)
    {
        if (!this.crashTime.ContainsKey(color))
        {
            this.LogError("Player {0} doesn't seem to be crashed", color);
            return int.MaxValue;
        }
        else
        {
            return this.crashTime[color] + this.SpawnTime - this.Tick;
        }
    }
}