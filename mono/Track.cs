using System.Collections.Generic;
using Newtonsoft.Json.Linq;

class Track
{
    private readonly string id;
    private readonly string name;
    private readonly IList<Piece> pieces;
    private readonly IList<Lane> lanes;

    private Track(string id, string name, List<Piece> pieces, List<Lane> lanes)
    {
        this.id = id;
        this.name = name;
        this.pieces = pieces.AsReadOnly();
        this.lanes = lanes.AsReadOnly();
    }

    public static Track FromData(JToken data)
    {
        var id = (string)data["id"];
        var name = (string)data["name"];
        var pieces = new List<Piece>();
        var lanes = new List<Lane>();
        var laneDict = new Dictionary<int, Lane>();
        var laneCount = 0;
        foreach (var lane in data["lanes"])
        {
            laneDict[(int)lane["index"]] = new Lane((double)lane["distanceFromCenter"]);
            laneCount += 1;
        }
        for (var i = 0; i < laneCount; i++)
        {
            lanes.Add(laneDict[i]);
        }
        foreach (var piece in data["pieces"])
        {
            var hasSwitch = piece["switch"] != null ? (bool)piece["switch"] : false;
            if (piece["radius"] != null)
            {
                pieces.Add(new BendPiece((double)piece["radius"], (double)piece["angle"], hasSwitch, lanes.AsReadOnly()));
            }
            else
            {
                pieces.Add(new StraightPiece((double)piece["length"], hasSwitch, lanes.AsReadOnly()));
            }
        }
        return new Track(id, name, pieces, lanes);
    }

    public string Id
    {
        get
        {
            return this.id;
        }
    }

    public string Name
    {
        get
        {
            return this.name;
        }
    }

    public IList<Piece> Pieces
    {
        get
        {
            return this.pieces;
        }
    }

    public IList<Lane> Lanes
    {
        get
        {
            return this.lanes;
        }
    }
}