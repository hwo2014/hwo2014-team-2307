using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

abstract class SendMessage
{
    private readonly JObject obj;

    public SendMessage(string type)
    {
        this.obj = new JObject();
        this.obj["msgType"] = type;
    }

    public SendMessage(string type, object data)
    {
        this.obj = new JObject();
        this.obj["msgType"] = type;
        this.obj["data"] = JToken.FromObject(data);
    }

    public string ToJson()
    {
        return JsonConvert.SerializeObject(this.obj);
    }
}