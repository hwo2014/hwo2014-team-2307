class JoinRaceMessage : SendMessage
{
    public JoinRaceMessage(string name, string key, string trackName, int carCount) : base("joinRace", new { botId = new { name = name, key = key }, trackName = trackName, carCount = carCount })
    {
    }

    public JoinRaceMessage(string name, string key, string trackName, string password, int carCount) : base("joinRace", new { botId = new { name = name, key = key }, trackName = trackName, password = password, carCount = carCount })
    {
    }
}