using System;
using Newtonsoft.Json.Linq;

class Position
{
    private readonly double angle;
    private readonly int lap;
    private readonly int index;
    private readonly double dist;
    private readonly int startLane;
    private readonly int endLane;
    private bool bumpedBehind = false;
    private bool bumpedFront = false;
    private readonly bool crashed;
    private readonly double throttleFactor;
    private readonly int turboRemaining;
    private readonly Track track;
    private readonly Position previous;

    public Position(JToken data, bool crashed, double throttleFactor, int turboRemaining, Track track, Position previous)
    {
        this.angle = (double)data["angle"];
        this.lap = (int)data["piecePosition"]["lap"];
        this.index = (int)data["piecePosition"]["pieceIndex"];
        this.dist = (double)data["piecePosition"]["inPieceDistance"];
        this.startLane = (int)data["piecePosition"]["lane"]["startLaneIndex"];
        this.endLane = (int)data["piecePosition"]["lane"]["endLaneIndex"];
        this.crashed = crashed;
        this.throttleFactor = throttleFactor;
        this.turboRemaining = turboRemaining;
        this.track = track;
        this.previous = previous;
    }

    public Position(Position previous, double angle, double speed, int scheduledSwitch, int? turboDuration, double? turboFactor)
    {
        this.angle = angle;
        this.lap = previous.lap;
        this.index = previous.index;
        this.dist = previous.dist + speed;
        this.startLane = previous.startLane;
        this.endLane = previous.endLane;
        this.crashed = false;
        this.throttleFactor = previous.throttleFactor;
        if (previous.turboRemaining > 0)
        {
            this.turboRemaining = previous.turboRemaining - 1;
            if (this.turboRemaining == 0)
            {
                this.throttleFactor = 1.0;
            }
        }
        else
        {
            this.turboRemaining = 0;
        }
        if (turboDuration.HasValue)
        {
            this.turboRemaining = turboDuration.Value;
            this.throttleFactor = turboFactor.Value;
        }
        this.track = previous.track;
        this.previous = previous;
        var len = this.Piece.GetApproxLength(this.startLane, this.endLane);
        if (this.dist >= len)
        {
            this.dist -= len;
            this.index += 1;
            if (this.index >= this.track.Pieces.Count)
            {
                this.index = 0;
                this.lap += 1;
            }
            this.startLane = this.endLane;
            if (this.Piece.HasSwitch)
            {
                this.endLane = Math.Min(Math.Max(this.endLane + scheduledSwitch, 0), this.track.Lanes.Count - 1);
            }
        }
    }

    public double Angle
    {
        get
        {
            return this.angle;
        }
    }

    public int Lap
    {
        get
        {
            return this.lap;
        }
    }

    public int Index
    {
        get
        {
            return this.index;
        }
    }

    public double Dist
    {
        get
        {
            return this.dist;
        }
    }

    public int StartLane
    {
        get
        {
            return this.startLane;
        }
    }

    public int EndLane
    {
        get
        {
            return this.endLane;
        }
    }

    public Piece Piece
    {
        get
        {
            return this.track.Pieces[this.index];
        }
    }

    public bool IsBumpedFromBehind
    {
        get
        {
            return this.bumpedBehind;
        }
    }

    public bool IsBumpedFromFront
    {
        get
        {
            return this.bumpedFront;
        }
    }

    public bool IsCrashed
    {
        get
        {
            return this.crashed;
        }
    }

    public double ThrottleFactor
    {
        get
        {
            return this.throttleFactor;
        }
    }

    public double TurboRemaining
    {
        get
        {
            return this.turboRemaining;
        }
    }

    public double AngleSpeed
    {
        get
        {
            if (this.crashed || previous == null)
            {
                return 0.0;
            }
            return this.angle - this.previous.angle;
        }
    }

    public double? Speed
    {
        get
        {
            if (this.crashed || this.previous == null)
            {
                return 0.0;
            }
            if (this.bumpedBehind || this.bumpedFront)
            {
                return null;
            }
            var speed = this.dist - this.previous.dist;
            if (this.index != this.previous.index)
            {
                var length = this.previous.Piece.GetLength(this.previous.startLane, this.previous.endLane);
                if (!length.HasValue)
                {
                    return null;
                }
                speed += length.Value;
            }
            return speed;
        }
    }

    public double? Curvature
    {
        get
        {
            return this.Piece.GetCurvature(this.startLane, this.endLane, this.dist);
        }
    }

    public double ApproxCurvature
    {
        get
        {
            return this.Piece.GetApproxCurvature(this.startLane, this.endLane, this.dist);
        }
    }

    public void CheckBumpState(Position other, Car myCar, Car otherCar)
    {
        if (this.crashed || other.crashed)
        {
            return;
        }
        var myOffset = myCar.Length - myCar.GuideFlagPosition;
        var otherOffset = otherCar.GuideFlagPosition;
        if (other.index == this.index && other.startLane == this.startLane && other.endLane == this.endLane && other.dist < this.dist)
        {
            if (other.dist + otherOffset + myOffset + 0.00000000001 >= this.dist)
            {
                this.bumpedBehind = true;
                other.bumpedFront = true;
            }
        }
        else if (other.index + 1 == this.index || (other.index + 1) % this.track.Pieces.Count == this.index && this.dist >= myOffset)
        {
            if (other.startLane == other.endLane && other.endLane == this.startLane || this.previous != null && this.previous.startLane == other.startLane && this.previous.endLane == other.endLane)
            {
                if (other.dist + otherOffset + myOffset - other.Piece.GetApproxLength(other.startLane, other.endLane) + 0.00000000001 >= this.dist)
                {
                    this.bumpedBehind = true;
                    other.bumpedFront = true;
                }
            }
        }
    }

    public void SetMeasuredSpeed(double speed)
    {
        if (this.previous != null && this.index != this.previous.index && !this.bumpedBehind && !this.crashed)
        {
            this.previous.Piece.SetMeasuredLength(this.previous.startLane, this.previous.endLane, this.previous.dist + speed - this.dist);
        }
    }
}