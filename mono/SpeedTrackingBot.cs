using System.Collections.Generic;
using Newtonsoft.Json.Linq;

abstract class SpeedTrackingBot : SpawnTimeTrackingBot
{
    private readonly Dictionary<int, IList<double>> progress = new Dictionary<int, IList<double>>();
    private readonly Dictionary<int, int> times = new Dictionary<int, int>();

    protected override void OnLapFinished(string name, string color, int lap, int lapTicks, int raceTicks, int rankOverall, int rankFastestLap)
    {
        base.OnLapFinished(name, color, lap, lapTicks, raceTicks, rankOverall, rankFastestLap);
        var id = this.GetCarIndex(color);
        if (!this.times.ContainsKey(id) || this.times[id] > lapTicks)
        {
            var list = new List<double>();
            this.progress[id] = list.AsReadOnly();
            this.times[id] = lapTicks;
            var positions = this.Histories[id].Positions;
            var startTick = -1;
            for (var tick = 0; tick < positions.Count; tick++)
            {
                var pos = positions[tick];
                if (pos.Lap == lap)
                {
                    if (startTick < 0)
                    {
                        startTick = tick;
                    }
                    list.Add(pos.Index + pos.Dist / pos.Piece.GetApproxLength(pos.StartLane, pos.EndLane));
                }
            }
        }
    }

    protected IList<double> GetBestLap(int carIndex)
    {
        if (this.progress.ContainsKey(carIndex))
        {
            return this.progress[carIndex];
        }
        else
        {
            return null;
        }
    }

    protected int GetEstimatedTime(int carIndex, Position pos, int endIndex)
    {
        var total = this.Track.Pieces.Count;
        var start = pos.Index + pos.Dist / pos.Piece.GetApproxLength(pos.StartLane, pos.EndLane);
        var end = endIndex;
        if (!this.progress.ContainsKey(carIndex))
        {
            return (int)(600 * (end - start) / total);
        }
        var prog = this.progress[carIndex];
        var time = 0;
        foreach (var d in prog)
        {
            if (d >= start && d < end || d + total < end)
            {
                time += 1;
            }
        }
        return time;
    }
}