using System;
using System.Collections.Generic;

class TestBot : SessionsBot
{
    protected override IEnumerable<Command> DriveInQualification()
    {
        var switchPlan = new SwitchPlan(this.Track);
        var turboPlan = new TurboPlan(this.Track);
        while (true)
        {
            if (this.Position.Lap > 0 && this.Position.Lap % 2 == 0 || this.LapCount.HasValue && this.Position.Lap + 1 == this.LapCount.Value)
            {
                yield return this.FastLap(switchPlan, turboPlan) ?? this.Oops();
            }
            else
            {
                yield return this.PrepareForFastLap(switchPlan, turboPlan) ?? this.Oops();
            }
        }
    }

    protected override IEnumerable<Command> DriveInMainRace()
    {
        var switchPlan = new SwitchPlan(this.Track);
        var turboPlan = new TurboPlan(this.Track);
        while (true)
        {
            if (this.LapCount.HasValue && this.Position.Lap + 1 == this.LapCount.Value)
            {
                yield return this.FastLap(switchPlan, turboPlan) ?? this.Oops();
            }
            else
            {
                yield return this.FastOverall(switchPlan, turboPlan) ?? this.Oops();
            }
        }
    }

    private Command Oops()
    {
        this.LogInfo("{0}: It seems that I'm about to crash", this.Tick);
        return this.SetThrottle(1.0);
    }

    private Command PrepareForFastLap(SwitchPlan switchPlan, TurboPlan turboPlan)
    {
        var fastSwitches = switchPlan.GetPlan(0, null, null);
        var switches = switchPlan.GetPlan(this.Position.Index + 1, this.Position.EndLane, fastSwitches.Item1, fastSwitches.Item2);
        var switchOptions = this.SwitchOptions(switchPlan, switches);
        var straights = turboPlan.MultiLapStraights;
        return this.DriveFast(switchOptions, straights, false);
    }

    private Command FastOverall(SwitchPlan switchPlan, TurboPlan turboPlan)
    {
        var switches = switchPlan.GetPlan(this.Position.Index + 1, this.Position.EndLane, null, null);
        var switchOptions = this.SwitchOptions(switchPlan, switches);
        var straights = turboPlan.MultiLapStraights;
        return this.DriveFast(switchOptions, straights, false);
    }

    private Command FastLap(SwitchPlan switchPlan, TurboPlan turboPlan)
    {
        var switches = switchPlan.GetPlan(this.Position.Index + 1, this.Position.EndLane, null);
        var switchOptions = this.SwitchOptions(switchPlan, switches);
        var straights = turboPlan.SingleLapStraights;
        return this.DriveFast(switchOptions, straights, true);
    }

    private IEnumerable<Dictionary<int, int>> SwitchOptions(SwitchPlan switchPlan, Tuple<int, int, Dictionary<int, int>> defaultSwitches)
    {
        if (defaultSwitches == null)
        {
            defaultSwitches = switchPlan.GetPlan(this.Position.Index + 1, this.Position.EndLane, null, null);
        }
        var switches = defaultSwitches.Item3;
        var nextSwitch = int.MaxValue;
        foreach (var kvp in switches)
        {
            if (kvp.Key > this.Position.Index && kvp.Key < nextSwitch)
            {
                nextSwitch = kvp.Key;
            }
        }
        if (nextSwitch == int.MaxValue)
        {
            return new [] { switches };
        }
        var defaultLane = this.Position.EndLane + switches[nextSwitch];
        var switchOptions = new List<Dictionary<int, int>>();
        foreach (var lane in this.LanePreferences(defaultLane, nextSwitch))
        {
            if (lane == defaultLane)
            {
                switchOptions.Add(switches);
            }
            else
            {
                var plan = switchPlan.GetPlan(nextSwitch + 1, lane, null, null);
                if (plan != null)
                {
                    plan.Item3[nextSwitch] = lane - this.Position.EndLane;
                    switchOptions.Add(plan.Item3);
                }
            }
        }
        return switchOptions;
    }

    private IEnumerable<int> LanePreferences(int defaultLane, int nextSwitch)
    {
        var options = new List<Tuple<int, double>>();
        for (var lane = defaultLane - 1; lane <= defaultLane + 1; lane++)
        {
            if (lane < 0 || lane >= this.Track.Lanes.Count)
            {
                continue;
            }
            options.Add(new Tuple<int, double>(lane, this.GetCollisionTime(nextSwitch, lane)));
        }
        options.Sort(delegate(Tuple<int, double> x, Tuple<int, double> y)
        {
            var a = x.Item2;
            var b = y.Item2;
            if (a == b)
            {
                if (x.Item1 == defaultLane)
                {
                    return -1;
                }
                else if (y.Item1 == defaultLane)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            return a > b ? -1 : 1;
        });
        foreach (var elem in options)
        {
            yield return elem.Item1;
        }
    }

    private double GetCollisionTime(int nextSwitch, int nextLane)
    {
        var res = double.PositiveInfinity;
        var pieces = this.Track.Pieces;
        var endIndex = nextSwitch + 1;
        while (!pieces[endIndex % pieces.Count].HasSwitch)
        {
            endIndex += 1;
        }
        var myTime = this.GetEstimatedTime(0, this.Position, endIndex);
        var histories = this.Histories;
        for (var i = 1; i < histories.Count; i++)
        {
            var pos = histories[i].CurrentPosition;
            if (pos.Index > this.Position.Index || pos.Index == this.Position.Index && pos.Dist > this.Position.Dist)
            {
                if (pos.EndLane == nextLane)
                {
                    var t = this.GetEstimatedTime(i, pos, endIndex);
                    if (t > myTime)
                    {
                        res = Math.Min(res, myTime - t);
                    }
                }
            }
        }
        return res;
    }

    private Command DriveFast(IEnumerable<Dictionary<int, int>> switchOptions, Dictionary<int, int> straights, bool stopAtFinishLine)
    {
        var pos = this.Position;
        if (!pos.Speed.HasValue)
        {
            this.LogInfo("{0}: Did I get bumped?", this.Tick);
            return this.SetThrottle(0.0);
        }
        foreach (var switches in switchOptions)
        {
            var nextSwitch = this.NextSwitch(pos, switches);
            if (nextSwitch != null && nextSwitch.Item2 == 0)
            {
                continue;
            }
            var cmd = this.TryTurbo(pos, switches, straights, nextSwitch, stopAtFinishLine) ?? this.TryOptimalThrottle(pos, switches, nextSwitch, stopAtFinishLine) ?? this.TrySwitch(pos, switches, nextSwitch, stopAtFinishLine);
            if (cmd != null)
            {
                return cmd;
            }
        }
        return null;
    }

    private Command TryTurbo(PositionWithThrottle pos, Dictionary<int, int> switches, Dictionary<int, int> straights, Tuple<int, int> nextSwitch, bool stopAtFinishLine)
    {
        if (!this.HasTurbo)
        {
            return null;
        }
        var startLap = pos.Lap;
        pos = this.SimulateSafeTick(pos, new TurboMessage(), startLap, stopAtFinishLine);
        if (pos == null)
        {
            return null;
        }
        if (pos.PreviousThrottle != 1.0)
        {
            pos = this.SimulateSafeTick(pos, new ThrottleMessage(1.0), startLap, stopAtFinishLine);
            if (pos == null)
            {
                return null;
            }
        }
        if (nextSwitch != null && pos.Index + (pos.Lap - startLap) * this.Track.Pieces.Count >= nextSwitch.Item1)
        {
            return null;
        }
        while (!straights.ContainsKey(pos.Index))
        {
            if (switches.ContainsKey(pos.Index + 1) && switches[pos.Index + 1] != 0)
            {
                pos = this.SimulateSafeTick(pos, new SwitchMessage(switches[pos.Index + 1]), startLap, stopAtFinishLine);
            }
            else
            {
                pos = this.SimulateSafeTick(pos, new PingMessage(), startLap, stopAtFinishLine);
            }
            if (pos == null)
            {
                return null;
            }
        }
        var index = pos.Index;
        while (pos.Index == index)
        {
            if (switches.ContainsKey(pos.Index + 1) && switches[pos.Index + 1] != 0)
            {
                pos = this.SimulateSafeTick(pos, new SwitchMessage(switches[pos.Index + 1]), startLap, stopAtFinishLine);
            }
            else
            {
                pos = this.SimulateSafeTick(pos, new PingMessage(), startLap, stopAtFinishLine);
            }
            if (pos == null)
            {
                return null;
            }
        }
        if (!this.IsSafeToThrottle(pos, switches, 0.0, null, startLap, stopAtFinishLine))
        {
            return null;
        }
        return this.Turbo();
    }

    private Command TryOptimalThrottle(PositionWithThrottle pos, Dictionary<int, int> switches, Tuple<int, int> nextSwitch, bool stopAtFinishLine)
    {
        if (!this.IsSafeToThrottle(pos, switches, 0.0, nextSwitch, pos.Lap, stopAtFinishLine))
        {
            return null;
        }
        double res;
        if (this.IsSafeToThrottle(pos, switches, 1.0, nextSwitch, pos.Lap, stopAtFinishLine))
        {
            res = 1.0;
        }
        else if (nextSwitch != null)
        {
            res = 0.0;
        }
        else
        {
            var min = 0.0;
            var max = 1.0;
            for (var i = 0; i < 20; i++)
            {
                var mid = (min + max) / 2;
                if (this.IsSafeToThrottle(pos, switches, mid, null, pos.Lap, stopAtFinishLine))
                {
                    min = mid;
                }
                else
                {
                    max = mid;
                }
            }
            res = min;
        }
        if (nextSwitch != null && pos.PreviousThrottle == res)
        {
            return this.Switch(nextSwitch.Item2);
        }
        else
        {
            return this.SetThrottle(res);
        }
    }

    private Command TrySwitch(PositionWithThrottle pos, Dictionary<int, int> switches, Tuple<int, int> nextSwitch, bool stopAtFinishLine)
    {
        if (nextSwitch == null)
        {
            return null;
        }
        var startLap = pos.Lap;
        pos = this.SimulateSafeTick(pos, new SwitchMessage(nextSwitch.Item2), startLap, stopAtFinishLine);
        if (pos == null)
        {
            return null;
        }
        if (!this.IsSafeToThrottle(pos, switches, 0.0, null, startLap, stopAtFinishLine))
        {
            return null;
        }
        return this.Switch(nextSwitch.Item2);
    }

    private bool IsSafeToThrottle(PositionWithThrottle pos, Dictionary<int, int> switches, double throttle, Tuple<int, int> nextSwitch, int startLap, bool stopAtFinishLine)
    {
        pos = this.SimulateSafeTick(pos, new ThrottleMessage(throttle), startLap, stopAtFinishLine);
        if (pos == null)
        {
            return false;
        }
        pos = this.SimulateSafeTick(pos, new ThrottleMessage(0.0), startLap, stopAtFinishLine);
        if (pos == null)
        {
            return false;
        }
        if (nextSwitch != null && pos.Index + (pos.Lap - startLap) * this.Track.Pieces.Count >= nextSwitch.Item1)
        {
            return false;
        }
        return this.IsSafeToFollow(pos, switches, startLap, stopAtFinishLine);
    }

    private bool IsSafeToFollow(PositionWithThrottle pos, Dictionary<int, int> switches, int startLap, bool stopAtFinishLine)
    {
        for (var i = 0; i < 100; i++)
        {
            if (switches.ContainsKey(pos.Index + 1) && switches[pos.Index + 1] != 0)
            {
                pos = this.SimulateSafeTick(pos, new SwitchMessage(switches[pos.Index + 1]), startLap, stopAtFinishLine);
            }
            else
            {
                pos = this.SimulateSafeTick(pos, new PingMessage(), startLap, stopAtFinishLine);
            }
            if (pos == null)
            {
                return false;
            }
        }
        return true;
    }

    private Tuple<int, int> NextSwitch(PositionWithThrottle pos, Dictionary<int, int> switches)
    {
        var nextSwitch = int.MaxValue;
        foreach (var kvp in switches)
        {
            if (kvp.Key > pos.Index && kvp.Key < nextSwitch)
            {
                nextSwitch = kvp.Key;
            }
        }
        if (nextSwitch == int.MaxValue)
        {
            return null;
        }
        var dir = switches[nextSwitch];
        if (dir == pos.PreviousScheduledSwitch)
        {
            return null;
        }
        if (dir == 0)
        {
            if (pos.EndLane == 0)
            {
                dir = -1;
            }
            else if (pos.EndLane == this.Track.Lanes.Count - 1)
            {
                dir = 1;
            }
            else
            {
                dir = 0;
            }
        }
        if (dir == pos.PreviousScheduledSwitch)
        {
            return null;
        }
        return new Tuple<int, int>(nextSwitch, dir);
    }

    private PositionWithThrottle SimulateSafeTick(PositionWithThrottle pos, SendMessage message, int startLap, bool stopAtFinishLine)
    {
        pos = this.SimulateTick(pos, message);
        if (pos == null)
        {
            return null;
        }
        if (!stopAtFinishLine || pos.Lap == startLap)
        {
            if (Math.Abs(pos.Angle) > this.SlipAngle)
            {
                return null;
            }
        }
        return pos;
    }
}