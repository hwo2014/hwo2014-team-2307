using System.Collections.Generic;
using Newtonsoft.Json.Linq;

class HistoryWithThrottle : History
{
    private readonly Track track;
    private readonly List<PositionWithThrottle> positions;
    private int scheduledSwitch;
    private double throttle;

    public HistoryWithThrottle(Track track) : base(track)
    {
        this.track = track;
        this.positions = new List<PositionWithThrottle>();
        this.scheduledSwitch = 0;
        this.throttle = 0.0;
    }

    public override void AddData(JToken data, bool crashed, double throttleFactor, int turboRemaining)
    {
        var pos = new PositionWithThrottle(data, crashed, throttleFactor, turboRemaining, this.scheduledSwitch, this.throttle, this.track, this.CurrentPosition);
        if (this.CurrentPosition != null && this.CurrentPosition.Index != pos.Index && pos.Piece.HasSwitch)
        {
            this.scheduledSwitch = 0;
        }
        base.AddPosition(pos);
        this.positions.Add(pos);
    }

    public new IList<PositionWithThrottle> Positions
    {
        get
        {
            return this.positions.AsReadOnly();
        }
    }

    public new PositionWithThrottle CurrentPosition
    {
        get
        {
            if (this.positions.Count > 0)
            {
                return this.positions[this.positions.Count - 1];
            }
            else
            {
                return null;
            }
        }
    }

    public new PositionWithThrottle PreviousPosition
    {
        get
        {
            if (this.positions.Count > 1)
            {
                return this.positions[this.positions.Count - 2];
            }
            else
            {
                return null;
            }
        }
    }

    public void SetSwitch(int direction)
    {
        this.scheduledSwitch = direction;
    }

    public void SetThrottle(double value)
    {
        this.throttle = value;
    }
}