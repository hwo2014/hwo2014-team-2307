using System;
using System.Threading.Tasks;

abstract class ProtocolBot
{
    private int tick = -1;

    public async Task RunAsync(string host, int port, SendMessage joinMessage)
    {
        this.LogInfo("Connecting to {0}:{1}", host, port);
        using(var client = new RaceClient(host, port))
        {
            await client.Send(joinMessage);
            while (this.tick < 0)
            {
                if (!await this.ReadMessages(client))
                {
                    this.LogError("Failed to initialize");
                    return;
                }
            }
            while (true)
            {
                var command = this.GetNextCommand();
                if (command == null)
                {
                    this.LogInfo("Quitting");
                    return;
                }
                await client.Send(command);
                if (!await this.ReadMessages(client))
                {
                    this.LogInfo("Race ended");
                    return;
                }
            }
        }
    }

    private async Task<bool> ReadMessages(RaceClient client)
    {
        var prevTick = this.tick;
        var hasPos = false;
        while (this.tick == prevTick || !hasPos)
        {
            var message = await client.Receive();
            if (message == null)
            {
                return false;
            }
            if (message.Tick.HasValue)
            {
                this.tick = message.Tick.Value;
            }
            if (message.Type == "carPositions")
            {
                hasPos = true;
            }
            this.ProcessMessage(message);
        }
        if (this.tick != prevTick + 1)
        {
            if (this.tick == 0)
            {
                this.LogInfo("Ticks reset from {0} to {1}", prevTick, this.tick);
            }
            else
            {
                this.LogError("Jumped from tick {0} to {1}", prevTick, this.tick);
            }
        }
        return true;
    }

    protected void LogInfo(string message, params object[] args)
    {
        Console.WriteLine(message, args);
    }

    protected void LogError(string message, params object[] args)
    {
        Console.Error.WriteLine("ERROR: " + message, args);
    }

    protected int Tick
    {
        get
        {
            return this.tick;
        }
    }

    abstract protected void ProcessMessage(RecvMessage message);
    abstract protected SendMessage GetNextCommand();
}