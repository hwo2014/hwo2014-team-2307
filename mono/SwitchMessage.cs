class SwitchMessage : SendMessage
{
    private readonly int direction;

    public SwitchMessage(int direction) : base("switchLane", direction > 0 ? "Right" : "Left")
    {
        this.direction = direction > 0 ? 1 : -1;
    }

    public int Direction
    {
        get
        {
            return this.direction;
        }
    }
}