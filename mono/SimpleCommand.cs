using System.Collections.Generic;

class SimpleCommand : Command
{
    private readonly SendMessage message;

    public SimpleCommand(SendMessage message)
    {
        this.message = message;
    }

    public override IEnumerable<SendMessage> GetMessages()
    {
        yield return this.message;
    }
}