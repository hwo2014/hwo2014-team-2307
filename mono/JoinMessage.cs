class JoinMessage : SendMessage
{
    public JoinMessage(string name, string key, string color) : base("join", new { name = name, key = key, color = color })
    {
    }
}