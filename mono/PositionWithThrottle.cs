using Newtonsoft.Json.Linq;

class PositionWithThrottle : Position
{
    private readonly int scheduledSwitch;
    private readonly double throttle;

    public PositionWithThrottle(JToken data, bool crashed, double throttleFactor, int turboRemaining, int scheduledSwitch, double throttle, Track track, Position previous) : base(data, crashed, throttleFactor, turboRemaining, track, previous)
    {
        this.scheduledSwitch = scheduledSwitch;
        this.throttle = throttle;
    }

    public PositionWithThrottle(Position previous, double angle, double speed, int scheduledSwitch, double throttle, int? turboDuration, double? turboFactor) : base(previous, angle, speed, scheduledSwitch, turboDuration, turboFactor)
    {
        if (previous != null && previous.Index != this.Index && this.Piece.HasSwitch)
        {
            this.scheduledSwitch = 0;
        }
        else
        {
            this.scheduledSwitch = scheduledSwitch;
        }
        this.throttle = throttle;
    }

    public int PreviousScheduledSwitch
    {
        get
        {
            return this.scheduledSwitch;
        }
    }

    public double PreviousThrottle
    {
        get
        {
            return this.throttle;
        }
    }
}