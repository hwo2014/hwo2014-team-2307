using System;
using System.Collections.Generic;

class BendPiece : Piece
{
    private readonly double radius;
    private readonly double angle;
    private readonly IList<Lane> lanes;

    public BendPiece(double radius, double angle, bool hasSwitch, IList<Lane> lanes) : base(hasSwitch)
    {
        this.radius = radius * Math.Sign(angle);
        this.angle = angle * Math.PI / 180;
        this.lanes = lanes;
    }

    public int Sign
    {
        get
        {
            return Math.Sign(this.angle);
        }
    }

    public double Angle
    {
        get
        {
            return Math.Abs(this.angle);
        }
    }

    public double GetRadius(int lane)
    {
        return Math.Abs(this.radius - this.lanes[lane].Offset);
    }

    public override double? GetCurvature(int startLane, int endLane, double dist)
    {
        if (startLane == endLane)
        {
            return Math.Sign(this.angle) / Math.Sqrt(Math.Abs(this.radius - this.lanes[startLane].Offset));
        }
        else
        {
            return null;
        }
    }

    public override double GetApproxCurvature(int startLane, int endLane, double dist)
    {
        var curv = this.GetCurvature(startLane, endLane, dist);
        if (curv.HasValue)
        {
            return curv.Value;
        }
        var r1 = Math.Abs(this.radius - this.lanes[startLane].Offset);
        var r2 = Math.Abs(this.radius - this.lanes[endLane].Offset);
        var cached = CurvCache.GetValue(r1, r2, Math.Abs(this.angle), dist);
        var r = 0.0;
        if (cached.HasValue)
        {
            r = cached.Value;
        }
        else
        {
            // Best guess
            dist /= this.GetApproxLength(startLane, endLane);
            dist = Math.Min(1.0, dist);
            var max = Math.Max(r1, r2);
            var min = Math.Min(r1, r2);
            if (r1 > r2)
            {
                dist = 1.0 - dist;
            }
            r = max * dist * dist - (min + max) * dist / 4.0 + min;
            // To make sure r is positive
            r = Math.Max(r, min / 2.0);
        }
        // To be a bit safer
        r *= 0.99;
        return Math.Sign(this.angle) / Math.Sqrt(r);
    }

    public void SetMeasuredCurvature(int startLane, int endLane, double dist, double curvature)
    {
        CurvCache.RecordValue(Math.Abs(this.radius - this.lanes[startLane].Offset), Math.Abs(this.radius - this.lanes[endLane].Offset), Math.Abs(this.angle), dist, 1.0 / curvature / curvature);
    }

    public override double? GetLength(int startLane, int endLane)
    {
        if (startLane == endLane)
        {
            var r = this.radius - this.lanes[startLane].Offset;
            return this.angle * r;
        }
        else
        {
            return LengthCache.GetBendValue(Math.Abs(this.radius - this.lanes[startLane].Offset), Math.Abs(this.radius - this.lanes[endLane].Offset), Math.Abs(this.angle));
        }
    }

    public override double GetApproxLength(int startLane, int endLane)
    {
        var length = this.GetLength(startLane, endLane);
        if (!length.HasValue)
        {
            var rs = this.radius - this.lanes[startLane].Offset;
            var re = this.radius - this.lanes[endLane].Offset;
            var d = Math.Sqrt(rs * rs + re * re - 2 * rs * re * Math.Cos(this.angle));
            var r = Math.Min(Math.Abs(rs), Math.Abs(re));
            length = 2 * r * Math.Asin(d / r / 2);
        }
        return length.Value;
    }

    public override void SetMeasuredLength(int startLane, int endLane, double length)
    {
        if (startLane != endLane)
        {
            LengthCache.SetBendValue(Math.Abs(this.radius - this.lanes[startLane].Offset), Math.Abs(this.radius - this.lanes[endLane].Offset), Math.Abs(this.angle), length);
        }
    }
}