class Lane
{
    private readonly double offset;

    public Lane(double offset)
    {
        this.offset = offset;
    }

    public double Offset
    {
        get
        {
            return this.offset;
        }
    }
}