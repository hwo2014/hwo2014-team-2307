using System;
using System.Collections.Generic;

class DelegateTestBot : CommandsBot
{
    protected override IEnumerable<Command> Drive()
    {
        yield return this.Delegate(base.Drive());
        yield return this.Switch(1);
        yield return this.Delegate(this.DriveSlow(100));
        yield return this.Delegate(this.DriveFast(100));
        yield return this.Delegate(this.Drive());
    }

    private IEnumerable<Command> DriveSlow(int ticks)
    {
        while (ticks --> 0)
        {
            yield return this.SetThrottle(0.5);
        }
    }

    private IEnumerable<Command> DriveFast(int ticks)
    {
        while (ticks --> 0)
        {
            yield return this.SetThrottle(0.63);
        }
    }
}