using System;
using System.Collections.Generic;

class SwitchPlan
{
    private readonly List<int> switches = new List<int>();
    private readonly List<int?[,]> path = new List<int?[,]>();
    private readonly List<double[,]> dist = new List<double[,]>();
    private readonly int laneCount;
    private readonly int pieceCount;

    public SwitchPlan(Track track)
    {
        var pieces = track.Pieces;
        var lanes = track.Lanes;
        var count = lanes.Count;
        var lensList = new List<double[,]>();
        var lens = new double[count, count];
        lensList.Add(lens);
        this.laneCount = count;
        this.pieceCount = pieces.Count;
        for (var p = 0; p < pieces.Count; p++)
        {
            var piece = pieces[p];
            var bend = piece as BendPiece;
            if (piece.HasSwitch)
            {
                for (var i = 0; i < lanes.Count; i++)
                {
                    for (var j = 0; j < lanes.Count; j++)
                    {
                        if (bend != null)
                        {
                            lens[i, j] += bend.Angle * Math.Sqrt(2.0 * Math.Max(bend.GetRadius(i), bend.GetRadius(j)));
                        }
                        else if (i != j)
                        {
                            lens[i, j] += (piece.GetApproxLength(i, j) - piece.GetApproxLength(i, i)) / 9;
                        }
                    }
                }
                this.switches.Add(p);
                this.path.Add(new int?[count, count]);
                this.dist.Add(new double[count, count]);
                lens = new double[count, count];
                lensList.Add(lens);
            }
            else if (bend != null)
            {
                for (var i = 0; i < lanes.Count; i++)
                {
                    var len = bend.Angle * Math.Sqrt(2.0 * bend.GetRadius(i));
                    for (var j = 0; j < lanes.Count; j++)
                    {
                        lens[i, j] += len;
                    }
                }
            }
        }
        this.dist.Add(new double[count, count]);
        var last = this.dist.Count - 1;
        for (var i = 0; i < lanes.Count; i++)
        {
            for (var j = 0; j < lanes.Count; j++)
            {
                if (i == j)
                {
                    this.dist[last][i, j] = lensList[last][i, j];
                }
                else
                {
                    this.dist[last][i, j] = double.PositiveInfinity;
                }
            }
        }
        for (var z = this.path.Count - 1; z >= 0; z--)
        {
            for (var i = 0; i < lanes.Count; i++)
            {
                for (var j = 0; j < lanes.Count; j++)
                {
                    var bestK = (int?)null;
                    var bestDist = double.PositiveInfinity;
                    for (var k = Math.Max(0, i - 1); k < Math.Min(lanes.Count, i + 2); k++)
                    {
                        var d = lensList[z][i, k] + this.dist[z + 1][k, j];
                        if (d < bestDist)
                        {
                            bestK = k;
                            bestDist = d;
                        }
                    }
                    this.path[z][i, j] = bestK - i;
                    this.dist[z][i, j] = bestDist;
                }
            }
        }
    }

    public Tuple<int, int, Dictionary<int, int>> GetPlan(int index, int? startLane, int? midLane, int? endLane)
    {
        var p1 = this.GetPlan(index, startLane, midLane);
        var p2 = this.GetPlan(0, midLane, endLane);
        if (p1 == null || p2 == null)
        {
            return null;
        }
        var dict = p1.Item3;
        foreach (var kvp in p2.Item3)
        {
            dict[kvp.Key + this.pieceCount] = kvp.Value;
        }
        return new Tuple<int, int, Dictionary<int, int>>(p1.Item1, p2.Item2, dict);
    }

    public Tuple<int, int, Dictionary<int, int>> GetPlan(int index, int? startLane, int? endLane)
    {
        var offset = 0;
        foreach (var p in this.switches)
        {
            if (p < index)
            {
                offset += 1;
            }
            else
            {
                break;
            }
        }
        var bestStart = 0;
        var bestEnd = 0;
        if (startLane.HasValue && endLane.HasValue)
        {
            bestStart = startLane.Value;
            bestEnd = endLane.Value;
        }
        else if (startLane.HasValue)
        {
            var best = double.PositiveInfinity;
            bestStart = startLane.Value;
            for (var j = 0; j < this.laneCount; j++)
            {
                if (this.dist[offset][bestStart, j] < best)
                {
                    best = this.dist[offset][bestStart, j];
                    bestEnd = j;
                }
            }
        }
        else if (endLane.HasValue)
        {
            var best = double.PositiveInfinity;
            bestEnd = endLane.Value;
            for (var i = 0; i < this.laneCount; i++)
            {
                if (this.dist[offset][i, bestEnd] < best)
                {
                    best = this.dist[offset][i, bestEnd];
                    bestStart = i;
                }
            }
        }
        else
        {
            var best = double.PositiveInfinity;
            for (var i = 0; i < this.laneCount; i++)
            {
                for (var j = 0; j < this.laneCount; j++)
                {
                    if (this.dist[offset][i, j] < best)
                    {
                        best = this.dist[offset][i, j];
                        bestStart = i;
                        bestEnd = j;
                    }
                }
            }
        }
        return this.MakePlan(offset, bestStart, bestEnd);
    }

    private Tuple<int, int, Dictionary<int, int>> MakePlan(int offset, int startLane, int endLane)
    {
        var plan = new Dictionary<int, int>();
        var pos = startLane;
        for (var z = offset; z < this.path.Count; z++)
        {
            var step = this.path[z][pos, endLane];
            if (step == null)
            {
                return null;
            }
            pos += step.Value;
            plan[this.switches[z]] = step.Value;
        }
        if (plan.Count == 0 && pos != endLane)
        {
            return null;
        }
        return new Tuple<int, int, Dictionary<int, int>>(startLane, endLane, plan);
    }
}