using System.Collections.Generic;
using Newtonsoft.Json.Linq;

abstract class SessionsBot : LengthsTrackingBot
{
    private bool qualification = true;
    private int? lapCount = null;
    private int? lapTimeLimit = null;
    private int? sessionTimeLimit = null;

    protected override IEnumerable<Command> Drive()
    {
        yield return this.Delegate(base.Drive());
        while (true)
        {
            if (this.qualification)
            {
                yield return this.DelegateWhile(this.DriveInQualification(), () => this.qualification);
                if (this.qualification)
                {
                    yield break;
                }
            }
            else
            {
                yield return this.DelegateWhile(this.DriveInMainRace(), () => !this.qualification);
                if (!this.qualification)
                {
                    yield break;
                }
            }
        }
    }

    protected abstract IEnumerable<Command> DriveInQualification();

    protected abstract IEnumerable<Command> DriveInMainRace();

    protected override void OnGameInit(JToken track, List<JToken> cars, JToken session)
    {
        base.OnGameInit(track, cars, session);
        this.qualification = true;
        this.lapCount = null;
        this.lapTimeLimit = null;
        this.sessionTimeLimit = null;
        if (session["quickRace"] != null)
        {
            this.qualification = (bool)session["quickRace"];
        }
        if (session["laps"] != null)
        {
            this.lapCount = (int)session["laps"];
        }
        if (session["maxLapTimeMs"] != null)
        {
            this.lapTimeLimit = (int)session["maxLapTimeMs"] * 60 / 1000;
        }
        if (session["durationMs"] != null)
        {
            this.sessionTimeLimit = (int)session["durationMs"] * 60 / 1000;
        }
    }

    protected bool IsQualification
    {
        get
        {
            return this.qualification;
        }
    }

    protected int? LapCount
    {
        get
        {
            return this.lapCount;
        }
    }

    protected int? LapTimeLimit
    {
        get
        {
            return this.lapTimeLimit;
        }
    }

    protected int? SessionTimeLimit
    {
        get
        {
            return this.sessionTimeLimit;
        }
    }
}