class ThrottleMessage : SendMessage
{
    private readonly double value;

    public ThrottleMessage(double value) : base("throttle", value)
    {
        this.value = value;
    }

    public double Value
    {
        get
        {
            return this.value;
        }
    }
}