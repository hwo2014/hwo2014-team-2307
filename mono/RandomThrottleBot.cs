using System;
using System.Collections.Generic;

class RandomThrottleBot : CommandsBot
{
    protected override IEnumerable<Command> Drive()
    {
        yield return this.Delegate(base.Drive());
        var random = new Random(0);
        while (true)
        {
            yield return this.SetThrottle(random.NextDouble());
        }
    }
}