using System;
using System.Collections.Generic;

class LengthCache
{
    private static readonly Dictionary<double, Dictionary<double, double>> straightValues = new Dictionary<double, Dictionary<double, double>>();
    private static readonly Dictionary<double, Dictionary<double, Dictionary<double, double>>> bendValues = new Dictionary<double, Dictionary<double, Dictionary<double, double>>>();

    static LengthCache()
    {
        straightValues[20.0] = new Dictionary<double, double>();
        straightValues[20.0][100.0] = 102.060274992934;
        straightValues[20.0][99.0] = 101.0804661468;
        straightValues[20.0][94.0] = 96.1876570838152;
        var a45 = 45.0 * Math.PI / 180;
        var a22 = 22.5 * Math.PI / 180;
        bendValues[a45] = new Dictionary<double, Dictionary<double, double>>();
        bendValues[a45][110.0] = new Dictionary<double, double>();
        bendValues[a45][110.0][90.0] = 81.0280595167185;
        bendValues[a45][90.0] = new Dictionary<double, double>();
        bendValues[a45][90.0][110.0] = 81.0294841420082;
        bendValues[a22] = new Dictionary<double, Dictionary<double, double>>();
        bendValues[a22][210.0] = new Dictionary<double, double>();
        bendValues[a22][210.0][190.0] = 81.0539041593052;
        bendValues[a22][190.0] = new Dictionary<double, double>();
        bendValues[a22][190.0][210.0] = 81.0546522063749;
    }

    public static void SetStraightValue(double length, double laneDist, double value)
    {
        if (!straightValues.ContainsKey(laneDist))
        {
            straightValues[laneDist] = new Dictionary<double, double>();
        }
        var dict = straightValues[laneDist];
        if (!dict.ContainsKey(length))
        {
            Console.WriteLine("Measured length for {0}/{1}: {2}", length, laneDist, value);
            dict[length] = value;
        }
        else
        {
            var d = Math.Abs(dict[length] - value);
            if (d > 0.00001)
            {
                Console.WriteLine("Bad length for {0}/{1}: {2} vs {3}", length, laneDist, value, dict[length]);
            }
        }
    }

    public static double? GetStraightValue(double length, double laneDist)
    {
        if (straightValues.ContainsKey(laneDist))
        {
            var dict = straightValues[laneDist];
            if (dict.ContainsKey(length))
            {
                return dict[length];
            }
        }
        return null;
    }

    public static void SetBendValue(double radius1, double radius2, double angle, double value)
    {
        if (!bendValues.ContainsKey(angle))
        {
            bendValues[angle] = new Dictionary<double, Dictionary<double, double>>();
        }
        var dict1 = bendValues[angle];
        if (!dict1.ContainsKey(radius1))
        {
            dict1[radius1] = new Dictionary<double, double>();
        }
        var dict2 = dict1[radius1];
        if (!dict2.ContainsKey(radius2))
        {
            Console.WriteLine("Measured length for {0}/{1}/{2}: {3}", radius1, radius2, angle, value);
            dict2[radius2] = value;
        }
        else
        {
            var d = Math.Abs(dict2[radius2] - value);
            if (d > 0.00001)
            {
                Console.WriteLine("Bad length for {0}/{1}/{2}: {3} vs {4}", radius1, radius2, angle, value, dict2[radius2]);
            }
        }
    }

    public static double? GetBendValue(double radius1, double radius2, double angle)
    {
        if (bendValues.ContainsKey(angle))
        {
            var dict1 = bendValues[angle];
            if (dict1.ContainsKey(radius1))
            {
                var dict2 = dict1[radius1];
                if (dict2.ContainsKey(radius2))
                {
                    return dict2[radius2];
                }
            }
        }
        return null;
    }
}