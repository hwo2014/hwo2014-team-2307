using System.Collections.Generic;
using Newtonsoft.Json.Linq;

class History
{
    private readonly Track track;
    private readonly List<Position> positions;

    public History(Track track)
    {
        this.track = track;
        this.positions = new List<Position>();
    }

    public virtual void AddData(JToken data, bool crashed, double throttleFactor, int turboRemaining)
    {
        this.AddPosition(new Position(data, crashed, throttleFactor, turboRemaining, this.track, this.CurrentPosition));
    }

    protected void AddPosition(Position pos)
    {
        this.positions.Add(pos);
    }

    public IList<Position> Positions
    {
        get
        {
            return this.positions.AsReadOnly();
        }
    }

    public Position CurrentPosition
    {
        get
        {
            if (this.positions.Count > 0)
            {
                return this.positions[this.positions.Count - 1];
            }
            else
            {
                return null;
            }
        }
    }

    public Position PreviousPosition
    {
        get
        {
            if (this.positions.Count > 1)
            {
                return this.positions[this.positions.Count - 2];
            }
            else
            {
                return null;
            }
        }
    }
}