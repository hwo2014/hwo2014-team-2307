using System.Threading.Tasks;

class Program
{
    public static void Main(string[] args)
    {
        var host = args[0];
        var port = int.Parse(args[1]);
        var name = args[2];
        var key = args[3];
        new TestBot().QuickRace(host, port, name, key).Wait();
    }

    private static void RunBots(string key, string track, params JoinBot[] bots)
    {
        var host = "prost.helloworldopen.com";
        var port = 8091;
        var name = "";
        var password = System.Guid.NewGuid().ToString();
        var tasks = new Task[bots.Length];
        tasks[0] = bots[0].HostRace(host, port, name, key, track, password, bots.Length);
        for (var i = 1; i < bots.Length; i++)
        {
            tasks[i] = bots[i].JoinRace(host, port, name, key, track, password, bots.Length);
        }
        Task.WaitAll(tasks);
    }
}