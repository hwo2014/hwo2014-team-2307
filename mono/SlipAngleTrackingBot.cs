using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

abstract class SlipAngleTrackingBot : PhysicsBot
{
    private double minSlipAngle = 0.0;
    private double maxSlipAngle = 90.0;

    protected override void OnCarPositions(List<JToken> positions)
    {
        base.OnCarPositions(positions);
        foreach (var history in this.Histories)
        {
            var angle = Math.Abs(history.CurrentPosition.Angle);
            this.minSlipAngle = Math.Max(this.minSlipAngle, angle);
            if (history.CurrentPosition.IsCrashed && history.PreviousPosition != null && !history.PreviousPosition.IsCrashed)
            {
                var next = this.SimulateTick(history.PreviousPosition, 0.0);
                if (next != null)
                {
                    this.maxSlipAngle = Math.Min(this.maxSlipAngle, Math.Abs(next.Angle));
                }
            }
        }
    }

    protected double MinSlipAngle
    {
        get
        {
            return this.minSlipAngle;
        }
    }

    protected double MaxSlipAngle
    {
        get
        {
            return this.maxSlipAngle;
        }
    }

    protected double SlipAngle
    {
        get
        {
            if (this.maxSlipAngle < 60)
            {
                return this.minSlipAngle;
            }
            else
            {
                return 59.999;
            }
        }
    }
}