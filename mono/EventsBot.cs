using System.Collections.Generic;
using Newtonsoft.Json.Linq;

abstract class EventsBot : JoinBot
{
    protected override void ProcessMessage(RecvMessage message)
    {
        switch (message.Type)
        {
            case "carPositions":
                this.OnCarPositions(this.ToList(message.Data));
                break;
            case "crash":
                this.OnCrash((string)message.Data["name"], (string)message.Data["color"]);
                break;
            case "createRace":
                this.OnCreateRace((string)message.Data["botId"]["name"], (string)message.Data["botId"]["key"]);
                break;
            case "dnf":
                this.OnDnf((string)message.Data["car"]["name"], (string)message.Data["car"]["color"], (string)message.Data["reason"]);
                break;
            case "finish":
                this.OnFinish((string)message.Data["name"], (string)message.Data["color"]);
                break;
            case "gameEnd":
                this.OnGameEnd(this.ToList(message.Data["results"]), this.ToList(message.Data["bestLaps"]));
                break;
            case "gameInit":
                this.OnGameInit(message.Data["race"]["track"], this.ToList(message.Data["race"]["cars"]), message.Data["race"]["raceSession"]);
                break;
            case "gameStart":
                this.OnGameStart();
                break;
            case "join":
                this.OnJoin((string)message.Data["name"], (string)message.Data["key"]);
                break;
            case "joinRace":
                this.OnJoinRace((string)message.Data["botId"]["name"], (string)message.Data["botId"]["key"]);
                break;
            case "lapFinished":
                this.OnLapFinished((string)message.Data["car"]["name"], (string)message.Data["car"]["color"], (int)message.Data["lapTime"]["lap"], (int)message.Data["lapTime"]["ticks"], (int)message.Data["raceTime"]["ticks"], (int)message.Data["ranking"]["overall"], (int)message.Data["ranking"]["fastestLap"]);
                break;
            case "spawn":
                this.OnSpawn((string)message.Data["name"], (string)message.Data["color"]);
                break;
            case "tournamentEnd":
                this.OnTournamentEnd();
                break;
            case "turboAvailable":
                this.OnTurboAvailable((int)message.Data["turboDurationTicks"], (double)message.Data["turboFactor"]);
                break;
            case "turboEnd":
                this.OnTurboEnd((string)message.Data["name"], (string)message.Data["color"]);
                break;
            case "turboStart":
                this.OnTurboStart((string)message.Data["name"], (string)message.Data["color"]);
                break;
            case "yourCar":
                this.OnYourCar((string)message.Data["name"], (string)message.Data["color"]);
                break;
            case "error":
                this.LogError("Message from server: {0}", (string)message.Data);
                break;
            default:
                this.LogError("Unknown message type {0}", message.Type);
                break;
        }
    }

    private List<JToken> ToList(JToken json)
    {
        var array = json as JArray;
        var list = new List<JToken>();
        foreach (var elem in array)
        {
            list.Add(elem);
        }
        return list;
    }

    protected virtual void OnCarPositions(List<JToken> positions)
    {
    }

    protected virtual void OnCrash(string name, string color)
    {
    }

    protected virtual void OnCreateRace(string name, string key)
    {
    }

    protected virtual void OnDnf(string name, string color, string reason)
    {
    }

    protected virtual void OnFinish(string name, string color)
    {
    }

    protected virtual void OnGameEnd(List<JToken> results, List<JToken> bestLaps)
    {
    }

    protected virtual void OnGameInit(JToken track, List<JToken> cars, JToken session)
    {
    }

    protected virtual void OnGameStart()
    {
    }

    protected virtual void OnJoin(string name, string key)
    {
    }

    protected virtual void OnJoinRace(string name, string key)
    {
    }

    protected virtual void OnLapFinished(string name, string color, int lap, int lapTicks, int raceTicks, int rankOverall, int rankFastestLap)
    {
    }

    protected virtual void OnSpawn(string name, string color)
    {
    }

    protected virtual void OnTournamentEnd()
    {
    }

    protected virtual void OnTurboAvailable(int duration, double factor)
    {
    }

    protected virtual void OnTurboEnd(string name, string color)
    {
    }

    protected virtual void OnTurboStart(string name, string color)
    {
    }

    protected virtual void OnYourCar(string name, string color)
    {
    }
}