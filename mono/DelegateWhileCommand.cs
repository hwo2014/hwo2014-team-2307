using System;
using System.Collections.Generic;

class DelegateWhileCommand : Command
{
    private readonly IEnumerable<Command> commands;
    private readonly Func<bool> condition;

    public DelegateWhileCommand(IEnumerable<Command> commands, Func<bool> condition)
    {
        this.commands = commands;
        this.condition = condition;
    }

    public override IEnumerable<SendMessage> GetMessages()
    {
        foreach (var command in this.commands)
        {
            foreach (var message in command.GetMessages())
            {
                if (condition() == false)
                {
                    yield break;
                }
                yield return message;
            }
        }
    }
}