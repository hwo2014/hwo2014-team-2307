using System;
using System.Collections.Generic;

abstract class PhysicsBot : SpeedTrackingBot
{
    private double power = 0.0;
    private double drag = 0.0;
    private double cx = 0.0;
    private double cy = 0.0;
    private double cf = 0.0;
    private double cg = 0.0;
    private double cfBound = double.PositiveInfinity;
    private int cxRetries = 15;
    private int cfRetries = 15;

    protected Command ThrottleToTarget(double targetSpeed)
    {
        var speed = this.Position.Speed;
        var targetThrottle = 0.0;
        if (speed.HasValue)
        {
            targetThrottle = (targetSpeed - this.drag * speed.Value) / this.power;
        }
        return this.SetThrottle(Math.Min(Math.Max(targetThrottle, 0.0), 1.0));
    }

    protected PositionWithThrottle SimulateTick(PositionWithThrottle previous, SendMessage message)
    {
        if (!previous.Speed.HasValue || this.cf == 0.0)
        {
            return null;
        }
        var v = previous.Speed.Value;
        var a = previous.Angle;
        var da = previous.AngleSpeed;
        var w = previous.ApproxCurvature;
        var scheduledSwitch = previous.PreviousScheduledSwitch;
        var throttle = previous.PreviousThrottle;
        var turboDuration = (int?)null;
        var turboFactor = (double?)null;
        if (message as PingMessage != null)
        {
            // Do nothing
        }
        else if (message as SwitchMessage != null)
        {
            scheduledSwitch = (message as SwitchMessage).Direction;
        }
        else if (message as ThrottleMessage != null)
        {
            throttle = (message as ThrottleMessage).Value;
        }
        else if (message as TurboMessage != null)
        {
            turboDuration = this.TurboDuration;
            turboFactor = this.TurboFactor;
        }
        else
        {
            this.LogError("Invalid message type: {0}", message.GetType().ToString());
        }
        var angle = a + da + Math.Max(0.0, this.cg * v * (this.cf * Math.Abs(w) * v - 1.0)) * Math.Sign(w) - this.cx * v * a - this.cy * da;
        var speed = this.drag * v + this.power * throttle * previous.ThrottleFactor;
        return new PositionWithThrottle(previous, angle, speed, scheduledSwitch, throttle, turboDuration, turboFactor);
    }

    protected Position SimulateTick(Position previous, double throttle)
    {
        if (!previous.Speed.HasValue || this.cf == 0.0)
        {
            return null;
        }
        var v = previous.Speed.Value;
        var a = previous.Angle;
        var da = previous.AngleSpeed;
        var w = previous.ApproxCurvature;
        var angle = a + da + Math.Max(0.0, this.cg * v * (this.cf * Math.Abs(w) * v - 1.0)) * Math.Sign(w) - this.cx * v * a - this.cy * da;
        var speed = this.drag * v + this.power * throttle * previous.ThrottleFactor;
        return new Position(previous, angle, speed, 0, null, null);
    }

    protected double Power
    {
        get
        {
            return this.power;
        }
    }

    protected double Drag
    {
        get
        {
            return this.drag;
        }
    }

    protected double X
    {
        get
        {
            return this.cx;
        }
    }

    protected double Y
    {
        get
        {
            return this.cy;
        }
    }

    protected double F
    {
        get
        {
            return this.cf;
        }
    }

    protected double G
    {
        get
        {
            return this.cg;
        }
    }

    protected override IEnumerable<Command> Drive()
    {
        yield return this.Delegate(base.Drive());
        yield return this.Delegate(this.MeasureAccCoeffs());
        yield return this.Delegate(this.MeasureSlipCoeffs());
        yield return this.Delegate(this.MeasureCentrifCoeffs());
    }

    private IEnumerable<Command> MeasureAccCoeffs()
    {
        var positions = this.History.Positions;
        for (var i = 0; true; i++)
        {
            while (positions.Count < i + 3)
            {
                yield return this.SetThrottle(1.0);
            }
            var s0 = positions[i].Speed;
            var s1 = positions[i + 1].Speed;
            var s2 = positions[i + 2].Speed;
            if (s0.HasValue && s1.HasValue && s2.HasValue && !positions[i].IsCrashed && !positions[i + 1].IsCrashed && !positions[i + 2].IsCrashed)
            {
                var v0 = s0.Value;
                var v1 = s1.Value;
                var v2 = s2.Value;
                var t0 = positions[i + 1].PreviousThrottle;
                var t1 = positions[i + 2].PreviousThrottle;
                var det = t0 - v0 * t1 / v1;
                if (!double.IsInfinity(det) && !double.IsNaN(det) && det != 0.0)
                {
                    this.power = (v1 - v0 * v2 / v1) / det;
                    this.drag = (t0 * v2 / v1 - t1) / det;
                    this.LogInfo("power={0} drag={1}", this.power, this.drag);
                    yield break;
                }
            }
        }
    }

    private IEnumerable<Command> MeasureSlipCoeffs()
    {
        var histories = this.Histories;
        for (var i = 0; true; i++)
        {
            foreach (var history in histories)
            {
                var positions = history.Positions;
                while (positions.Count < i + 4)
                {
                    yield return this.SlipCommand();
                }
                var p0 = positions[i];
                var p1 = positions[i + 1];
                var p2 = positions[i + 2];
                var p3 = positions[i + 3];
                if (p0.IsCrashed || p1.IsCrashed || p2.IsCrashed || p3.IsCrashed)
                {
                    continue;
                }
                var c0 = p0.Curvature;
                var c1 = p1.Curvature;
                var c2 = p2.Curvature;
                if (!c0.HasValue || !c1.HasValue || !c2.HasValue)
                {
                    continue;
                }
                if (c0.Value != c1.Value || c0.Value != c2.Value)
                {
                    continue;
                }
                var s0 = p0.Speed;
                var s1 = p1.Speed;
                var s2 = p2.Speed;
                if (!s0.HasValue || !s1.HasValue || !s2.HasValue)
                {
                    continue;
                }
                if (s0.Value * 1.00001 < s1.Value || s0.Value > 1.00001 * s1.Value || s0.Value * 1.00001 < s2.Value || s0.Value > 1.00001 * s2.Value)
                {
                    continue;
                }
                var a0 = p0.Angle;
                var a1 = p1.Angle;
                var a2 = p2.Angle;
                var da0 = p0.AngleSpeed;
                var da1 = p1.AngleSpeed;
                var da2 = p2.AngleSpeed;
                var da3 = p3.AngleSpeed;
                var dda0 = da1 - da0;
                var dda1 = da2 - da1;
                var dda2 = da3 - da2;
                var det = (a0 - a1) * (da0 - da2) - (a0 - a2) * (da0 - da1);
                var detX = (dda1 - dda0) * (da0 - da2) - (dda2 - dda0) * (da0 - da1);
                var detY = (a0 - a1) * (dda2 - dda0) - (a0 - a2) * (dda1 - dda0);
                if (Math.Abs(det) > 0.00001)
                {
                    this.cx = detX / det / s0.Value;
                    this.cy = detY / det;
                    this.LogInfo("cx={0} cy={1}", this.cx, this.cy);
                    if ((Math.Abs(this.cx - 0.00125) > 0.0000001 || Math.Abs(this.cy - 0.1) > 0.0000001) && this.cxRetries > 0)
                    {
                        this.cx = 0.0;
                        this.cy = 0.0;
                        this.LogInfo("Data looks suspicious");
                        this.cxRetries -= 1;
                        continue;
                    }
                    yield break;
               }
            }
        }
    }

    private Command SlipCommand()
    {
        foreach (var history in this.Histories)
        {
            if (!history.CurrentPosition.IsCrashed && history.CurrentPosition.Angle == 0.0)
            {
                var pos = history.PreviousPosition;
                if (!pos.IsCrashed && pos.Angle == 0.0 && pos.Curvature.HasValue && pos.Curvature.Value != 0.0 && pos.Speed.HasValue)
                {
                    this.cfBound = Math.Min(this.cfBound, 1.0 / pos.Speed.Value / Math.Abs(pos.Curvature.Value));
                }
            }
        }
        if (this.Position.Curvature.HasValue && this.Position.Curvature.Value != 0.0 && this.Position.Angle != 0.0)
        {
            var speed = this.Position.Speed;
            if (speed.HasValue)
            {
                return this.ThrottleToTarget(speed.Value);
            }
        }
        if (this.Position.ApproxCurvature != 0.0)
        {
            return this.SetThrottle(1.0);
        }
        var curr = this.Position.Index;
        var lane = this.Position.EndLane;
        for (var idx = (curr + 1) % this.Track.Pieces.Count; idx != curr; idx = (idx + 1) % this.Track.Pieces.Count)
        {
            var w = Math.Abs(this.Track.Pieces[idx].GetApproxCurvature(lane, lane, 0.0));
            if (w != 0.0)
            {
                var target = Math.Max(1.1 / this.cfBound / w, 0.5 * this.power / (1.0 - this.drag));
                return this.ThrottleToTarget(target);
            }
        }
        return this.SetThrottle(1.0);
    }

    private IEnumerable<Command> MeasureCentrifCoeffs()
    {
        var histories = this.Histories;
        for (var i = 0; true; i++)
        {
            foreach (var history in histories)
            {
                var positions = history.Positions;
                while (positions.Count < i + 3)
                {
                    yield return this.SetThrottle(1.0);
                }
                var p0 = positions[i];
                var p1 = positions[i + 1];
                var p2 = positions[i + 2];
                if (p0.IsCrashed || p1.IsCrashed || p2.IsCrashed)
                {
                    continue;
                }
                if (!p0.Speed.HasValue || !p1.Speed.HasValue)
                {
                    continue;
                }
                var v0 = p0.Speed.Value;
                var v1 = p1.Speed.Value;
                var a0 = p0.Angle;
                var a1 = p1.Angle;
                var da0 = p0.AngleSpeed;
                var da1 = p1.AngleSpeed;
                var da2 = p2.AngleSpeed;
                var dda0 = da1 - da0;
                var dda1 = da2 - da1;
                if (dda0 == 0.0 || dda1 == 0.0)
                {
                    continue;
                }
                var m0 = dda0 + this.cx * v0 * a0 + this.cy * da0;
                var m1 = dda1 + this.cx * v1 * a1 + this.cy * da1;
                if (Math.Abs(m0 / dda0) < 0.0001 || Math.Abs(m1 / dda1) < 0.0001)
                {
                    continue;
                }
                if (!p0.Curvature.HasValue || !p1.Curvature.HasValue)
                {
                    continue;
                }
                var w0 = Math.Abs(p0.Curvature.Value);
                var w1 = Math.Abs(p1.Curvature.Value);
                if (w0 == 0.0 || w1 == 0.0)
                {
                    continue;
                }
                var det = (m1 * v0 * v0 * w0 - m0 * v1 * v1 * w1);
                if (Math.Abs(det / m1 / v0) < 0.0001)
                {
                    continue;
                }
                this.cf = (m1 * v0 - m0 * v1) / det;
                if (v0 != 0.0)
                {
                    this.cg = Math.Sign(p0.Curvature.Value) * m0 / v0 / (this.cf * w0 * v0 - 1.0);
                }
                else
                {
                    this.cg = Math.Sign(p1.Curvature.Value) * m1 / v1 / (this.cf * w1 * v1 - 1.0);
                }
                this.LogInfo("cf={0} cg={1}", 1.0 / this.cf / this.cf, this.cg);
                if (this.cf < 0 || this.cg < 0)
                {
                    this.cf = 0.0;
                    this.cg = 0.0;
                    this.LogError("There's definitely something wrong here");
                    continue;
                }
                if (Math.Abs(this.cg - 0.3) > 0.0000001 && this.cfRetries > 0)
                {
                    this.cf = 0.0;
                    this.cg = 0.0;
                    this.LogInfo("Data looks suspicious");
                    this.cfRetries -= 1;
                    continue;
                }
                this.BackfillCurvMeasurements();
                yield break;
            }
        }
    }

    protected override void OnCarPositions(List<Newtonsoft.Json.Linq.JToken> positions)
    {
        base.OnCarPositions(positions);
        if (this.cf != 0.0)
        {
            foreach (var history in this.Histories)
            {
                if (history.PreviousPosition != null)
                {
                    this.FillCurvMeasurements(history.PreviousPosition, history.CurrentPosition);
                }
            }
        }
    }

    private void BackfillCurvMeasurements()
    {
        foreach (var history in this.Histories)
        {
            var positions = history.Positions;
            for (var i = 1; i < positions.Count; i++)
            {
                this.FillCurvMeasurements(positions[i - 1], positions[i]);
            }
        }
    }

    private void FillCurvMeasurements(Position pos, Position next)
    {
        var bend = pos.Piece as BendPiece;
        if (pos.Speed.HasValue && !pos.IsCrashed && !next.IsCrashed && pos.StartLane != pos.EndLane && bend != null)
        {
            var v = pos.Speed.Value;
            var a = pos.Angle;
            var da = pos.AngleSpeed;
            var res = next.Angle;
            var diff = res + this.cx * v * a + this.cy * da - a - da;
            if (Math.Abs(diff) > 1e-6)
            {
                var w = (diff * bend.Sign / this.cg / v + 1.0) / this.cf / v;
                bend.SetMeasuredCurvature(pos.StartLane, pos.EndLane, pos.Dist, w);
            }
        }
    }
}