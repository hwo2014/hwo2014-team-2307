class CreateRaceMessage : SendMessage
{
    public CreateRaceMessage(string name, string key, string trackName, int carCount) : base("createRace", new { botId = new { name = name, key = key }, trackName = trackName, carCount = carCount })
    {
    }

    public CreateRaceMessage(string name, string key, string trackName, string password, int carCount) : base("createRace", new { botId = new { name = name, key = key }, trackName = trackName, password = password, carCount = carCount })
    {
    }
}