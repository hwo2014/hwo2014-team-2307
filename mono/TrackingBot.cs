using System.Collections.Generic;
using Newtonsoft.Json.Linq;

abstract class TrackingBot : CommandsBot
{
    private string ownId = null;
    private Track track = null;
    private Dictionary<string, int> carMapping = new Dictionary<string, int>();
    private List<Car> cars = new List<Car>();
    private List<History> histories = new List<History>();
    private List<bool> crashed = new List<bool>();
    private List<double> turboed = new List<double>();
    private List<int> turboRemaining = new List<int>();
    private bool turboAvailable = false;
    private int turboTicks = 0;
    private double turboFactor = 1.0;

    protected override SendMessage GetNextCommand()
    {
        var message = base.GetNextCommand();
        var switchMessage = message as SwitchMessage;
        if (switchMessage != null)
        {
            this.History.SetSwitch(switchMessage.Direction);
        }
        var throttleMessage = message as ThrottleMessage;
        if (throttleMessage != null)
        {
            this.History.SetThrottle(throttleMessage.Value);
        }
        var turboMessage = message as TurboMessage;
        if (turboMessage != null)
        {
            // Just to make sure we don't keep spamming turbo for some reason
            this.turboAvailable = false;
        }
        return message;
    }

    protected override void OnYourCar(string name, string color)
    {
        base.OnYourCar(name, color);
        this.ownId = color;
        this.carMapping[this.ownId] = 0;
        this.cars.Add(null);
        this.histories.Add(null);
        this.crashed.Add(false);
        this.turboed.Add(1.0);
        this.turboRemaining.Add(0);
    }

    protected override void OnGameInit(JToken track, List<JToken> cars, JToken session)
    {
        base.OnGameInit(track, cars, session);
        this.track = Track.FromData(track);
        foreach (var car in cars)
        {
            var id = (string)car["id"]["color"];
            if (!this.carMapping.ContainsKey(id))
            {
                this.carMapping[id] = this.carMapping.Count;
                this.cars.Add(null);
                this.histories.Add(null);
                this.crashed.Add(false);
                this.turboed.Add(1.0);
                this.turboRemaining.Add(0);
            }
            var idx = this.carMapping[id];
            this.cars[idx] = new Car((double)car["dimensions"]["length"], (double)car["dimensions"]["width"], (double)car["dimensions"]["guideFlagPosition"]);
            this.histories[idx] = id == this.ownId ? new HistoryWithThrottle(this.track) : new History(this.track);
            this.crashed[idx] = false;
            this.turboed[idx] = 1.0;
            this.turboRemaining[idx] = 0;
        }
    }

    protected override void OnCarPositions(List<JToken> positions)
    {
        base.OnCarPositions(positions);
        foreach (var pos in positions)
        {
            var id = (string)pos["id"]["color"];
            var idx = this.carMapping[id];
            if (this.turboRemaining[idx] > 0)
            {
                this.turboRemaining[idx] -= 1;
            }
            this.histories[idx].AddData(pos, this.crashed[idx], this.turboed[idx], this.turboRemaining[idx]);
        }
        for (var i = 0; i < this.histories.Count; i++)
        {
            for (var j = 0; j < this.histories.Count; j++)
            {
                if (i != j)
                {
                    this.histories[i].CurrentPosition.CheckBumpState(this.histories[j].CurrentPosition, this.cars[i], this.cars[j]);
                }
            }
        }
    }

    protected override void OnCrash(string name, string color)
    {
        base.OnCrash(name, color);
        this.crashed[this.carMapping[color]] = true;
    }

    protected override void OnDnf(string name, string color, string reason)
    {
        base.OnDnf(name, color, reason);
        this.crashed[this.carMapping[color]] = true;
    }

    protected override void OnFinish(string name, string color)
    {
        base.OnFinish(name, color);
        this.crashed[this.carMapping[color]] = true;
    }

    protected override void OnSpawn(string name, string color)
    {
        base.OnSpawn(name, color);
        this.crashed[this.carMapping[color]] = false;
        if (color == this.ownId)
        {
            this.History.SetThrottle(0.0);
            this.turboAvailable = false;
        }
    }

    protected override void OnTurboAvailable(int duration, double factor)
    {
        base.OnTurboAvailable(duration, factor);
        this.turboAvailable = true;
        this.turboTicks = duration + 1;
        this.turboFactor = factor;
    }

    protected override void OnTurboEnd(string name, string color)
    {
        base.OnTurboEnd(name, color);
        var idx = this.carMapping[color];
        this.turboed[idx] = 1.0;
        if (this.turboRemaining[idx] != 1)
        {
            this.LogError("Tick {0}, Player \"{1}\": Bad turbo counter, should still have {2} ticks left", this.Tick, name, this.turboRemaining[idx] - 1);
            this.turboRemaining[idx] = 0;
        }
    }

    protected override void OnTurboStart(string name, string color)
    {
        base.OnTurboStart(name, color);
        var idx = this.carMapping[color];
        this.turboed[idx] = this.turboFactor;
        this.turboRemaining[idx] = this.turboTicks + 1;
        if (color == this.ownId)
        {
            this.turboAvailable = false;
        }
    }

    protected int GetCarIndex(string color)
    {
        return this.carMapping[color];
    }

    protected bool HasTurbo
    {
        get
        {
            return this.turboAvailable && this.turboRemaining[0] == 0;
        }
    }

    protected IList<History> Histories
    {
        get
        {
            return this.histories.AsReadOnly();
        }
    }

    protected HistoryWithThrottle History
    {
        get
        {
            return this.histories[0] as HistoryWithThrottle;
        }
    }

    protected PositionWithThrottle Position
    {
        get
        {
            return this.History.CurrentPosition;
        }
    }

    protected double Throttle
    {
        get
        {
            return this.Position.PreviousThrottle;
        }
    }

    protected Track Track
    {
        get
        {
            return this.track;
        }
    }

    protected int TurboDuration
    {
        get
        {
            return this.turboTicks;
        }
    }

    protected double TurboFactor
    {
        get
        {
            return this.turboFactor;
        }
    }
}