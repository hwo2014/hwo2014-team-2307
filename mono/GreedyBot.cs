using System;
using System.Collections.Generic;

class GreedyBot : LengthsTrackingBot
{
    protected override IEnumerable<Command> Drive()
    {
        yield return this.SetThrottle(1.0);
        yield return this.Switch(1);
        yield return this.Delegate(base.Drive());
        while (true)
        {
            var min = 0.0;
            var max = 1.0;
            var limit = 59.999;
            if (this.HasTurbo && this.Throttle == 1.0 && this.SimTurboBump(false) < limit && this.SimTurboBump(true) >= limit)
            {
                yield return this.Turbo();
            }
            if (this.SimBump(1.0) < limit)
            {
                yield return this.SetThrottle(1.0);
                continue;
            }
            for (var i = 0; i < 0; i++)
            {
                var mid = (min + max) / 2;
                if (this.SimBump(mid) < limit)
                {
                    min = mid;
                }
                else
                {
                    max = mid;
                }
            }
            yield return this.SetThrottle(min);
        }
    }

    private double SimBump(double throttle, int ticks = 1)
    {
        var pos = this.Position;
        var res = Math.Abs(pos.Angle);
        for (var i = 0; i < ticks; i++)
        {
            pos = this.SimulateTick(pos, new ThrottleMessage(throttle));
            if (pos == null)
            {
                return 100.0;
            }
            res = Math.Max(res, Math.Abs(pos.Angle));
        }
        for (var i = 0; i < 80; i++)
        {
            pos = this.SimulateTick(pos, new ThrottleMessage(0.0));
            if (pos == null)
            {
                return 100.0;
            }
            res = Math.Max(res, Math.Abs(pos.Angle));
        }
        return res;
    }

    private double SimTurboBump(bool addTick)
    {
        var pos = this.Position;
        var res = Math.Abs(pos.Angle);
        if (addTick)
        {
            pos = this.SimulateTick(pos, new ThrottleMessage(1.0));
            if (pos == null)
            {
                return 100.0;
            }
            res = Math.Max(res, Math.Abs(pos.Angle));
        }
        pos = this.SimulateTick(pos, new TurboMessage());
        res = Math.Max(res, Math.Abs(pos.Angle));
        for (var i = 0; i < this.TurboDuration; i++)
        {
            pos = this.SimulateTick(pos, new ThrottleMessage(1.0));
            if (pos == null)
            {
                return 100.0;
            }
            res = Math.Max(res, Math.Abs(pos.Angle));
        }
        for (var i = 0; i < 80; i++)
        {
            pos = this.SimulateTick(pos, new ThrottleMessage(0.0));
            if (pos == null)
            {
                return 100.0;
            }
            res = Math.Max(res, Math.Abs(pos.Angle));
        }
        return res;
    }
}