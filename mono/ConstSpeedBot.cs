using System.Collections.Generic;

class ConstSpeedBot : PhysicsBot
{
    private double speed;

    public ConstSpeedBot(double speed)
    {
        this.speed = speed;
    }

    protected override IEnumerable<Command> Drive()
    {
        yield return this.Delegate(base.Drive());
        while (true)
        {
            yield return this.ThrottleToTarget(this.speed);
        }
    }
}