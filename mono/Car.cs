class Car
{
    private readonly double length;
    private readonly double width;
    private readonly double guideFlagPosition;

    public Car(double length, double width, double guideFlagPosition)
    {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }

    public double Length
    {
        get
        {
            return this.length;
        }
    }

    public double Width
    {
        get
        {
            return this.width;
        }
    }

    public double GuideFlagPosition
    {
        get
        {
            return this.guideFlagPosition;
        }
    }
}