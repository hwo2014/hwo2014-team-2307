using System;
using System.Collections.Generic;

abstract class CommandsBot : EventsBot
{
    private readonly IEnumerator<SendMessage> messages;

    public CommandsBot()
    {
        this.messages = this.Delegate(this.Drive()).GetMessages().GetEnumerator();
    }

    protected override SendMessage GetNextCommand()
    {
        if (this.messages.MoveNext() == false)
        {
            this.LogInfo("Drive function returned before the race was over");
            return null;
        }
        return this.messages.Current;
    }

    protected Command Delegate(IEnumerable<Command> driver)
    {
        return new DelegateCommand(driver);
    }

    protected Command DelegateWhile(IEnumerable<Command> driver, Func<bool> condition)
    {
        return new DelegateWhileCommand(driver, condition);
    }

    protected Command Quit()
    {
        return new SimpleCommand(null);
    }

    protected Command SetThrottle(double value)
    {
        return new SimpleCommand(new ThrottleMessage(value));
    }

    protected Command Switch(int direction)
    {
        return new SimpleCommand(new SwitchMessage(direction));
    }

    protected Command Turbo()
    {
        return new SimpleCommand(new TurboMessage());
    }

    protected Command Wait()
    {
        return new SimpleCommand(new PingMessage());
    }

    protected virtual IEnumerable<Command> Drive()
    {
        yield break;
    }
}