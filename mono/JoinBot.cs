using System.Threading.Tasks;

abstract class JoinBot : ProtocolBot
{
    public async Task QuickRace(string host, int port, string name, string key)
    {
        this.LogInfo("Starting a quick race");
        await this.RunAsync(host, port, new JoinMessage(name, key, "blue"));
    }

    public async Task QuickRace(string host, int port, string name, string key, string trackName)
    {
        this.LogInfo("Starting a quick race on track {0}", trackName);
        await this.RunAsync(host, port, new JoinRaceMessage(name, key, trackName, 1));
    }

    public async Task JoinRace(string host, int port, string name, string key, string trackName, int carCount)
    {
        this.LogInfo("Joining a public race on track {0} with {1} players", trackName, carCount);
        await this.RunAsync(host, port, new JoinRaceMessage(name, key, trackName, carCount));
    }

    public async Task JoinRace(string host, int port, string name, string key, string trackName, string password, int carCount)
    {
        this.LogInfo("Joining a private race on track {0} with {1} players", trackName, carCount);
        await this.RunAsync(host, port, new JoinRaceMessage(name, key, trackName, password, carCount));
    }

    public async Task HostRace(string host, int port, string name, string key, string trackName, string password, int carCount)
    {
        this.LogInfo("Hosting a private race on track {0} with {1} players", trackName, carCount);
        await this.RunAsync(host, port, new CreateRaceMessage(name, key, trackName, password, carCount));
    }
}