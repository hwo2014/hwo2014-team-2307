using System.Collections.Generic;

abstract class Command
{
    public abstract IEnumerable<SendMessage> GetMessages();
}